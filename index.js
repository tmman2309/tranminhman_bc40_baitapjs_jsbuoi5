// ex1
function ex1KhuVuc(khuvuc){
    switch(khuvuc){
        case "A": {
            return 2;
        };
        case "B": {
            return 1;
        };
        case "C": {
            return 0.5;
        };
        case "X": {
            return 0;
        };
        default: {
            alert("Vui lòng chọn khu vực!")
            return 0;
        }
    }
};

function ex1DoiTuong(doituong){
    switch(doituong){
        case "1": {
            return 2.5;
        };
        case "2": {
            return 1.5;
        };
        case "3": {
            return 1;
        };
        case "0": {
            return 0;
        };
        default: {
            alert("Vui lòng chọn đối tượng!");
            return 0;
        }
    }
};

function ex1KetQua(){
    var khuVuc = document.getElementById("ex1-khuvuc").value;
    var doiTuong = document.getElementById("ex1-doituong").value;
    var diemChuan = document.getElementById("ex1-diemchuan").value *1;
    var diemMon1 = document.getElementById("ex1-diemmon1").value *1;
    var diemMon2 = document.getElementById("ex1-diemmon2").value *1;
    var diemMon3 = document.getElementById("ex1-diemmon3").value *1;
    var thongBao = document.getElementById("ex1-thongbao");

    var tongDiem = diemMon1 + diemMon2 + diemMon3 + ex1KhuVuc(khuVuc) + ex1DoiTuong(doiTuong);

    if(diemChuan < 0 || diemChuan >30 || diemMon1 < 0 || diemMon1 > 10 || diemMon2 < 0 || diemMon2 > 10 || diemMon3 < 0 || diemMon3 > 10){
        alert("Vui lòng nhập đúng điểm!")
    } else if(diemChuan <= tongDiem && diemMon1 > 0 && diemMon2 > 0 && diemMon3 >0){
        thongBao.innerText = `👉 Bạn đã đậu. Tổng điểm: ${tongDiem}`;
    } else if(diemMon1 == 0 || diemMon2 == 0 || diemMon3 ==0){
        thongBao.innerText = `👉 Bạn đã rớt. Do có điểm nhỏ hơn hoặc bằng 0`;
    } else{
        thongBao.innerText = `👉 Bạn đã rớt. Tổng điểm: ${tongDiem}`;
    };
};


// ex2
function ex2TinhTienDien(){
    var hoTen = document.getElementById("ex2-hoten").value;
    var soKW = document.getElementById("ex2-sokw").value *1;
    var result = 0;
    var thongBao = document.getElementById("ex2-thongbao");

    if(soKW <= 50){
        result = soKW*500;
    } else if(soKW <=100){
        result = 50*500 + (soKW-50)*650;
    } else if(soKW<=200){
        result = 50*500 + 50*650 + (soKW -100)*850;
    } else if(soKW<=350){
        result = 50*500 + 50*650 + 100*850 + (soKW - 200)*1100;
    } else{
        result = 50*500 + 50*650 + 100*850 + 150*1100 + (soKW - 350)*1300;
    };

    thongBao.innerText = `👉 Họ tên: ${hoTen}; Tiền điện: ${new Intl.NumberFormat('vn-VN', { style: 'currency', currency: 'VND' }).format(result)}`;
}